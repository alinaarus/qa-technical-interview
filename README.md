# QA Technical Interview Challenge

This challenge aims to identify your degree of autonomy in performing a task. We believe that this quality is essential for carrying out daily activities, and we want to see how you do. The challenge will be a small project in which you will have to write a test that controls a browser and performs a series of actions. When you finish at least one of the challenges, send us your bitbucket repo and you will be invited to a technical interview that will consist of questions related to your project. You can find the requirements below and some hints to get you started. Best of luck!

**What is browser automation?**
Browser automation is the technique of programmatically launching a web application in a browser and automatically executing various actions, just as a regular user would. Browser testing gives you the speed and efficiency that would be impossible for a human tester. Protractor, Cypress, and Selenium are some of the popular tools used in-browser testing.

Some activities performed in browser automation are as follows:

*  Navigate to the application URL and make sure it launches
*  Test the various links on the web page and ensure they are not broken
*  Keep a record of the broken links on the page
*  Perform load and performance testing on your web application
*  Launch multiple instances of the browsers with different test users and ensure that concurrent actions work as expected

**What are the project rules?**

1. You need to do the task by yourself without asking for help from someone else
2. Use internet to find answers to your questions
3. You need to be able to understand and explain how your code works
4. Submit your bitbucket repo with at least one challenge solved util 11 January 2022 to hr@tickbird.org
---

## 1. Setup
1.  Create a free account on Bitbucket and [fork this repository](https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/)
2.  Make sure your repository is public. You can read how to do this [here](https://confluence.atlassian.com/bitbucketserver/allowing-public-access-to-code-776639799.html)
3.  Clone your repository to your local machine
4.  Open the cloned folder in your preferred code editor. There are no requirements on this but some software can assist you with hints regarding your code like "Visual Studio Code" or you can use "Notepad++". Whatever you are familiar with
5.  Chose a programing language, you will have to pick one from: Python, Javascript (Node.js) or Java. From a personal point of view I found Python the easiest one
6.  Install the programing language on your PC
7.  Install Selenium Server and Selenium Clients or WebDriver Language Bindings. More information [here](https://www.selenium.dev/downloads/)
---

## 2. Challenge 1
1.  Open the URL "https://tickbird-dev.web.app" in the browser
2.  Fill the "Contact Us" form on the bottom of the page
3.  Submit the form
4.  Verify that the "Thank you" message appears after the form was submitted"
---

## 3. Challenge 2
1.  Open the URL "https://tickbird-dev.web.app" in the browser
2.  Click on the menu icon in the top right corner
3.  Click on "Careers" from the menu
4.  On the "Careers" page fill the "Application" form and submit
5.  Verify that the "Thank you" message appears after the form was submitted
---

## 4. Extra points Challenge
1. Take a look at [Cucumber](https://cucumber.io/docs/installation/)
2. Read about Page Object Model in Selenium
3. Structure your code into different modules
4. Write the 2 challenges above in Gherkin Syntax using the Page Object Model and try to use parameters in your steps when repeating the same action with different values
5. Run the test in parallel on different browsers

## Guidelines
Challenges 1 and 2 can be simple scripts. The written code must follow principles below:

1. Standards of indentation and formatting are followed, so that the code and its structure are clearly visible
2. Variables are named meaningfully, so that they communicate intent
3. Comments, which are present only where needed, are concise and adhere to standard formats
4. Guard clauses are used instead of nested if statements
6. Functions are short and to the point, and do one thing
7. Indirection is minimized as much as possible, while still maintaining flexibility

Unreadable
```
        try:
          if item[1][2]=='1':
            qtytype='0'
            qty=str(item[1][0])
            items=item[1][0]
            amt='0.0'
            tot_amt=str(float(item[1][1])/100)
#              tot_amt=str(float(int(item[1][0])*int(item[1][1]))/100)
          else:
            qtytype='1'
            items=item[1][0]
            amt='0.0'
            tot_amt=str(float(item[1][1])/100)
            qty=0
            for entry in item[1][4]:
              qty+=entry[0]
        except TypeError:
          eType, eValue, eTraceback = sys.exc_info()
          print >> sys.stderr, time.strftime("%Y-%m-%d %H:%M:%S"), str(traceback.format_exception(eType,eValue,eTraceback))
```

Readable
```
item_approved = (item['approved'] == '1' and item['active'] == '1')

if item_approved:
    try:
        db_item = item.get_db_record(item['id'])
    except DoesNotExist:
        db_item = item.create_db_record(**item)

    item.approve(db_item)
    db_item.save()

    publish_event(item.events.APPROVAL)
```

For any questions please use the email address hr@tickbird.org.